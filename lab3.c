#include <stdio.h>
#include <stdlib.h>
#include "../include/cis1057.h"

/*
 * Programmer:    << put your name here >>
 * Class:         Introduction to C Programming 1057 Spring 2019
                  Section 004      
 * Assignment:    Number 3 – Create my header file
 * Date:          << put today's date here >>
 * Version:       1 
 * Description:   Create and test out our header file.
 * File:          lab3.c
 */

const int MY_SPEED_IN_KILOMETERS = 100;

int main()
{
	// Create and print out my header
	const char *my_banner = "This program was created by " 
	    PROGRAMMER_NAME " at " ORGANIZATION "\nfor the " LAB 
	    " department.";
	
	printf( "\n%s\n", my_banner );
	puts( "(c) 2019 " PROGRAMMER_NAME );

	// Print out the Temple slogan
	printf( "\n%s!\n\n", TEMPLE_SLOGAN );

	// Let's drive to the moon!
	printf( "The moon is %d kilometers away.\n", 
		AVERAGE_DISTANCE_EARTH_TO_MOON_IN_KILOMETERS );

	printf( "Driving at %d kilometers an hour, it would take %d hours\n"
		"to get to the moon.\n\n",
			MY_SPEED_IN_KILOMETERS,
			AVERAGE_DISTANCE_EARTH_TO_MOON_IN_KILOMETERS / 100 );

	return EXIT_SUCCESS;
}
