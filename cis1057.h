#ifndef CIS1057_H
#define CIS1057_H

/*
 * Description:    Header file to use for spring 2019 C programming.
 * Programmer:     << put your name here >>
 * Date:           << put today's date here >>
 * File:	        cis1057.h
 */ 

#define PROGRAMMER_NAME     "Ray Lauff"           // your name here
#define ORGANIZATION        "Temple University"
#define LAB                 "Computer Science"
#define ASSIGNED_CLASS      "CIS 1057 Spring 2019"
#define EMAIL_ADDRESS       "labwork@temple.edu"  // your email here

#define YARDS_IN_MILE       1760
#define DEGREES_IN_CIRCLE    360
#define NUMBER_OF_US_STATES   50
#define ONE_CM_IN_INCHES       0.3937
#define ONE_INCH_IN_CM         2.54	

#define MY_FAVORITE_NUMBER	7000              // your number here

const char *TEMPLE_MASCOT          = "Stella the Owl";
const char *TEMPLE_MASCOT_TWITTER  = "@StellaEPZ";
const char *TEMPLE_SLOGAN          = "Go Owls";

const int AVERAGE_DISTANCE_EARTH_TO_MOON_IN_KILOMETERS      = 384400;
const int AVERAGE_DISTANCE_EARTH_TO_MOON_IN_MILES           = 238900;
const int AVERAGE_DISTANCE_PHILADELPHIA_TO_LA_IN_KILOMETERS = 3852;
const int AVERAGE_DISTANCE_PHILADELPHIA_TO_LA_IN_MILES      = 2393;

const int DAYS_IN_JANUARY                              = 31;
const int DAYS_IN_FEBRUARY                             = 28;
const int DAYS_IN_MARCH                                = 31;

// 4567890123456789012345678901234567890123456789012345678901234567890
//       1111111111222222222233333333334444444444555555555566666666667

#endif // CIS1057_H 

